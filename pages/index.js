import Head from 'next/head';
import { useState, useEffect } from 'react';
import { proxyBase, baseURL } from '../services/proxyBase';
import fetch from 'isomorphic-unfetch';

const Home = ({ posts }) => {
  const [todos, setTodos] = useState(posts);
  const [newTodo, setNewTodo] = useState('');
  const [updateTodo, setUpdateTodo] = useState({ id: null, text: null });
  const [error, setError] = useState('');
  const changeToDo = text => {
    setNewTodo(text);
  };

  // useEffect(() => {
  //   if (!todos) {
  //     console.log('UseEffect');
  //     proxyBase.get('api/posts').then(response => {
  //       if (response.data.success) {
  //         setTodos(response.data.posts);
  //       }
  //     });
  //   }
  // }, []);

  const keyPressed = event => {
    if (event.key === 'Enter' && newTodo.length) {
      proxyBase.post('api/post', { post: newTodo }).then(response => {
        if (response.data.success) {
          if (todos && todos.length) {
            setTodos([...todos, response.data.post]);
          } else {
            setTodos([response.data.post]);
          }
        } else {
          setError('Cant create');
        }
      });
    }
  };

  const saveUpdated = () => {
    if (updateTodo.id) {
      proxyBase
        .put(`api/post/${updateTodo.id}`, { post: updateTodo.text })
        .then(response => {
          if (response.data.success) {
            const newTodos = todos.map(element => {
              if (element._id === updateTodo.id) {
                element.post = updateTodo.text;
                element.status = 'incomplete';
              }
              return element;
            });
            setTodos(newTodos);
            setUpdateTodo({ id: null, text: null });
          } else {
            setError('Cant update');
          }
        });
    }
  };

  const changeStatus = id => {
    if (id) {
      proxyBase.put(`api/post/${id}`, { status: 'done' }).then(response => {
        if (response.data.success) {
          const newTodo = todos.map(element => {
            if (element._id === id) {
              element.status = 'done';
            }
            return element;
          });
          setTodos(newTodo);
        } else {
          setError('Cant update');
        }
      });
    }
  };

  const updateToDo = (id, text) => {
    setUpdateTodo({ id, text });
  };

  const deleteTodo = id => {
    if (id) {
      proxyBase.delete(`api/post/${id}`).then(response => {
        if (response.data.success) {
          const newTodo = todos.filter(element => element._id !== id);
          setTodos(newTodo);
        } else {
          setError('Cant delete');
        }
      });
    }
  };

  return (
    <>
      <Head>
        <title>My Todo list</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <div>
        <center>
          <h1>Todo List</h1>
          <p>Save with enter</p>
          <p className='small'>✔: Mark complete</p>
          <p className='small'>o: Save edit</p>
          <p className='small'>x: delete Todo</p>
        </center>
        <error>{error}</error>
        <div className='content'>
          <div className='header'>
            <input
              type='text'
              onChange={e => changeToDo(e.target.value)}
              onKeyPress={keyPressed}
              placeholder='Add todo here!'
            />
          </div>

          {todos && todos.length ? (
            todos.map(todo => (
              <div key={todo._id} className='todoItem'>
                <span
                  className={todo.status}
                  suppressContentEditableWarning={true}
                  contentEditable='true'
                  onInput={e =>
                    updateToDo(todo._id, e.currentTarget.textContent)
                  }
                >
                  {todo.post}
                </span>
                <button onClick={() => deleteTodo(todo._id)}>
                  <span>x</span>
                </button>
                <button onClick={saveUpdated}>
                  <span className=''>o</span>
                </button>
                {todo.status === 'incomplete' && (
                  <button onClick={() => changeStatus(todo._id)}>
                    <span className=''>✔</span>
                  </button>
                )}
              </div>
            ))
          ) : (
            <p>0 posts :c</p>
          )}
        </div>
      </div>
    </>
  );
};

Home.getInitialProps = async () => {
  try {
    const res = await fetch(`${baseURL}api/posts`);
    const data = await res.json();
    if (data) {
      const { posts } = data;
      return { posts };
    }
    return { posts: null };
  } catch (error) {
    console.log(error);
    return { posts: null };
  }
};

export default Home;
