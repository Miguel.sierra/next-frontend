import axios from 'axios';

let baseURL = 'https://nodeco-todo.herokuapp.com/';

const getConfigAxios = () => {
  return {
    baseURL
  };
};

const instance = axios.create(getConfigAxios());
module.exports = { proxyBase: instance, baseURL };
